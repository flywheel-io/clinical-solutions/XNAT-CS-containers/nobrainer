#!/usr/bin/env python
import os
import sys
import subprocess
import traceback

from fw_nobrainer.parser import parse_config
from fw_nobrainer.xnat_logging import stdout_log, stderr_log

def main() -> None:
    # try-except + traceback + exit for any error that may occur
    try:
        # find input NIFTI
        if os.path.exists('/input/NIFTI'):
            scan_nii_files=os.listdir("/input/NIFTI")
            nifti_files = [file for file in scan_nii_files if file.endswith('.nii') or file.endswith('.nii.gz')]
            input_nifti=nifti_files[0]

        else:
            stdout_log.error("NIFTI scan resource not found. Exiting.")
            sys.exit(1)

        # get configs        
        configs_list,output_file=parse_config(input_nifti)

        cmd_list=["nobrainer","predict"]
        cmd_list.extend(configs_list)
        
        input_nifti_fp=f"/input/NIFTI/{input_nifti}"
        output_fp=f"/output/{output_file}"

        cmd_list.append(input_nifti_fp)
        cmd_list.append(output_fp)
        
        cmd_to_run = ' '.join(cmd_list)
        stdout_log.info("Running:  %s",cmd_to_run)

        process = subprocess.Popen(
                cmd_list,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=True
            )
        
        # Stream and log the stdout
        for line in process.stdout:
            stdout_log.info(line.strip())

        process.wait()
        return_code = process.returncode

        # when --verbose is disabled
        if "--verbose" not in cmd_list:
            if os.path.isfile(output_fp):
                stdout_log.info(f"Output saved to {output_fp}")

        # if non-zero return code, log to stderr and exit
        if return_code != 0:
            stdout_log.error(f"Nobrainer returned a non-zero return code of {return_code}. Exiting! Check standard error log for more details.")
            for line in process.stderr:
                stderr_log.error(line.strip())

            sys.exit(1)
    except Exception as exp:
        traceback_info = traceback.format_exc()
        stdout_log.error("There was an exception: %s Check stderr.log for more details. \n",exp)
        stderr_log.error("Exception: %s %s \n",exp,traceback_info)
        sys.exit(1) 

if __name__ == "__main__":
    main()
