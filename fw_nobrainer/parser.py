import argparse
import json
import sys
from fw_nobrainer.xnat_logging import stdout_log
from typing import List, Tuple

parser = argparse.ArgumentParser()
parser.add_argument('--verbose', required=False, action='store_true')
parser.add_argument('--output-file', required=False)
parser.add_argument('--model', required=True, default='brainy')
# commenting out for now. Future models that get added in may leverage these two configs better.
# parser.add_argument('--block-shape', required=False, default=(128, 128, 128),type=int,nargs=3)
# parser.add_argument('--resize-features-to', required=False, default=(256, 256, 256),type=int,nargs=3)
parser.add_argument('--threshold', required=True, default=0.3,type=float)
parser.add_argument('--largest-label', action='store_true')
parser.add_argument('--rotate-and-predict', action='store_true')

# Parse the command line arguments
args = parser.parse_args()

def parse_config(input_nifti: str) -> Tuple[List[str], str]:
    """ 
    Parse configs
    
    Args:
        input_nifti (str): input NIfTI filename

    Returns:
         Tuple[List[str], str]:
            A tuple containing:
                - List[str]: List of configs for Nobrainer command.
                - str: output file name.
    """

    configs = []
    output_file = None 

    if args.threshold <= 0 or args.threshold >= 1:
        stdout_log.error("Threshold must be greater than 0 and less than 1.")
        sys.exit(1)

    # create a list of the configs
    for arg in vars(args):
        argument_name = '--' + arg.replace('_', '-')
        argument_value = getattr(args, arg)

        if arg == 'output_file':
            # If output_file left empty, go with default naming
            if argument_value is None or argument_value == '':
                nifti_suffix=input_nifti.split(".")[0]
                output_file = f'{nifti_suffix}_brainmask.nii.gz'
            # use the user-provided output-file
            else:
                nifti_suffix=argument_value.split(".")[0]
                output_file = f"{nifti_suffix}.nii.gz"
        # map the model chosen to the .h5 model file
        elif arg == 'model':
            with open("/work/fw_nobrainer/model_mappings.json", 'r') as file:
                model_mappings = json.load(file)
            model_file = model_mappings.get(argument_value)

            configs.append(f'{argument_name}=/work/models/{model_file}')

        elif isinstance(argument_value, bool):
            if argument_value:
                configs.append(argument_name)

        elif isinstance(argument_value, list):
            if arg in ['block_shape', 'resize_features_to']:
                configs.append(argument_name)
                configs.extend(map(str, argument_value))
            else:
                configs.append(argument_name)
                configs.append(' '.join(map(str, argument_value)))
        else:
            configs.extend([argument_name, str(argument_value)])

    return configs, output_file