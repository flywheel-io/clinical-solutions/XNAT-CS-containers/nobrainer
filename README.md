# Nobrainer

[Nobrainer](https://github.com/neuronets/nobrainer/tree/1.1.1) is a deep learning framework for 3D image processing. It implements several 3D convolutional models from recent literature, methods for loading and augmenting volumetric data that can be used with any TensorFlow or Keras model, losses and metrics for 3D data, and simple utilities for model training, evaluation, prediction, and transfer learning. 

This scan-level XNAT container wraps Nobrainer v1.1.1 and embeds [brainy](https://github.com/neuronets/brainy/tree/0.1.0), a model  used for predicting a brain mask on a T1w image. This container is based off Flywheel's [Nobrainer gear](https://github.com/flywheel-apps/nobrainer-gear/tree/master).


- [Nobrainer](#nobrainer)
  - [Summary](#summary)
  - [Inputs](#inputs)
  - [Output](#output)
  - [Citation](#citation)

## Summary
- The input must be a T1w NIFTI file (.nii or .nii.gz) that is saved as a scan resource under the NIFTI folder. The container will specifically look for a folder called NIFTI under the scan, and assumes that there is only 1 NIFTI file (`Nobrainer` will always take the first .nii or .nii.gz file found as the input file).
  -  <img src="./images/prereq.png" width=35% />
- The container embeds [brainy](https://github.com/neuronets/brainy) but other [pre-trained models](https://github.com/neuronets/trained-models/tree/master) can be embedded in the future.

- If `Nobrainer` is rerun on a scan with the same *output_file* name, any pre-existing [brain mask](#output) will be overwritten

- Performance
  - `reserve-memory` of 1 GB was set in the `command.json`.
  - Runtime will be much faster when a GPU is available compared to a CPU.
  - Performance of brain mask prediction will vary based off the quality of the input T1w and shape. [brainy](https://github.com/neuronets/brainy) was trained on images that were 256x256x256, 1mm isotropic in voxel size.

  
## Inputs

- *model*
  - __Description__: *Pre-trained model.*
  - __Default__: `brainy`
  
- *verbose*
  - __Description__: *Switch on progress logs.*
  - __Default__: `False`
  
- *largest_label*
  - __Description__: *Zero out all values not connected to the largest contiguous label (not  including 0 values). This remove false positives in binary prediction.*
  - __Default__: `False`
  
- *rotate_and_predict*
  - __Description__: *Average the prediction with a prediction on a rotated (and subsequently un-rotated) volume. This can produce a better overall prediction.*
  - __Default__: `False`

- *output_file*
  - __Description__: *Name of output brain mask.*
  - __Default__: Takes the suffix of the input NIFTI file and appends *brainmask* to the filename, i.e *{input_NIFTI_suffix}_brainmask.nii.gz*.
 
- *threshold*
  - __Description__: *Threshold used to binarize model output. Only used in binary prediction and must be greater than 0 and less than 1.*
  - __Default__: `0.3`


## Output
`Nobrainer` will save the brain mask in the **nobrainer** scan resource:
  - <img src="./images/output_fs.png" width=35% />



## Citation
Kaczmarzyk, J., McClure, P., Zulfikar, W., Rana, A., Rajaei, H., Richie-Halford, A., Bansal, S., Jarecka, D., Lee, J., & Ghosh, S. (2023). neuronets/nobrainer: 1.1.1 (1.1.1). Zenodo. https://doi.org/10.5281/zenodo.8417516




